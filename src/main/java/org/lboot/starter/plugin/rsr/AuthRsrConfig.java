package org.lboot.starter.plugin.rsr;

import cn.hutool.core.lang.Validator;
import cn.hutool.core.util.ClassUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.lboot.core.auth.anno.CheckPerm;
import org.lboot.core.auth.anno.CheckRole;

import java.lang.reflect.Method;
import java.util.*;

/**
 * @author kindear
 * 鉴权资源获取
 */
public interface AuthRsrConfig {
    // 返回扫描包
    String scanPackage();

    default Map<String,List<ResTreeNode>> loadRsr(){
        Map<String,List<ResTreeNode>> resMap = new HashMap<>();
        Set<Class<?>> classes = ClassUtil.scanPackage(scanPackage());
        for (Class<?> clazz:classes){
            RsrTree rbac = clazz.getAnnotation(RsrTree.class);
            if (Validator.isNotEmpty(rbac)){
                List<ResTreeNode> nodes = resMap.get(rbac.value());
                if (Validator.isEmpty(nodes)){
                    nodes = new ArrayList<>();
                }
                Api api = clazz.getAnnotation(Api.class);
                ResTreeNode node = new ResTreeNode();
                if (Validator.isNotEmpty(api)){
                    node.setGroup(rbac.value());
                    node.setLabel(api.tags()[0]);
                    node.setValue(api.tags()[0]);
                    node.setExt(clazz.getName());
                    node.setDisabled(true);
                }
                List<ResTreeNode> children = new ArrayList<>();
                if (Validator.isNotEmpty(rbac)){
                    //log.info(clazz.getName());
                    Method[] methods = clazz.getMethods();
                    for (Method method:methods){
                        ResTreeNode c = new ResTreeNode();
                        // 使用忽略注解，则不生成权限树
                        RsrIgnore rsrIgnore = method.getAnnotation(RsrIgnore.class);
                        // 设置忽略权限树生成
                        ApiOperation apiOperation = method.getAnnotation(ApiOperation.class);
                        if (Validator.isEmpty(rsrIgnore) && Validator.isNotEmpty(apiOperation)){
                            CheckRole checkRole = method.getAnnotation(CheckRole.class);
                            if (Validator.isNotEmpty(checkRole)){
                                // 如果为空，则为管理员专属权限
                                String orPerm = checkRole.orPerm();
                                if (Validator.isNotEmpty(orPerm)){
                                    c.setLabel(apiOperation.value());
                                    c.setValue(orPerm);
                                    c.setExt(method.getName());
                                    c.setGroup(rbac.value());
                                    c.setDisabled(false);
                                    children.add(c);
                                }
                            }
                            // 权限校验
                            CheckPerm checkPerm = method.getAnnotation(CheckPerm.class);
                            if (Validator.isNotEmpty(checkPerm)){
                                c.setLabel(apiOperation.value());
                                c.setValue(checkPerm.value());
                                c.setExt(method.getName());
                                c.setGroup(rbac.value());
                                c.setDisabled(false);
                                children.add(c);
                            }
                        }

                    }
                }
                node.setChildren(children);
                nodes.add(node);
                resMap.put(rbac.value(), nodes);
            }else {
                continue;
            }
        }
        return resMap;
    }

}

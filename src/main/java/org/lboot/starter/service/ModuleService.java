package org.lboot.starter.service;

/**
 * @author kindear
 * 模块服务 获取模块相关信息
 */
public interface ModuleService {
    /**
     * 获取模块名称
     * @return 模块名称
     */
    String moduleName();

    /**
     * 获取模块版本
     * @return 模块版本
     */
    String moduleVersion();

}

package org.lboot.starter.anno;
import org.springframework.core.annotation.AliasFor;

import java.lang.annotation.*;

/**
 * 表单重复提交锁定
 * @author Administrator
 * 通过 IP 和 登录 Token 双重锁定
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD,ElementType.TYPE})
public @interface FormLock {

    /**
     * 锁定key
     */
    @AliasFor("value")
    String key() default "ALL";

    @AliasFor("key")
    String value() default "ALL";

    /**
     * 锁定时长
     */
    int sec() default 5;

}

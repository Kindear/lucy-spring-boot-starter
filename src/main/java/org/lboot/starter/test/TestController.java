package org.lboot.starter.test;

import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.lboot.core.domain.ResponseDTO;
import org.lboot.starter.anno.FormLock;
import org.lboot.starter.plugin.rsr.AuthRsrService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("test")
@AllArgsConstructor
public class TestController {
    AuthRsrService rsrService;
    // 资源选择树
    @GetMapping("res/tree")
    @ApiOperation(value = "资源树")
    public ResponseDTO<Object> resTree(){
        return ResponseDTO.succData(
                rsrService.rsrTree()
        );
    }

    @GetMapping("res/tree/v2")
    @ApiOperation(value = "资源树v2")
    public ResponseDTO<Object> resTreeV2(@RequestParam("groups") List<String> groups){
        //
        return ResponseDTO.succData(
                rsrService.rsrTree(groups)
        );
    }

    @FormLock("form-lock")
    @PostMapping("formlock")
    public ResponseDTO<Object> formLockTest(){
        return ResponseDTO.succData("测试成功");
    }

}

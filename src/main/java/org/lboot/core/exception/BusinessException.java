package org.lboot.core.exception;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.lboot.core.domain.ErrMsg;
import org.springframework.http.HttpStatus;

/**
 * @author kindear
 * 自定义异常类
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class BusinessException extends RuntimeException{
    Integer code;

    ErrMsg errMsg;

    public BusinessException(){}

    public BusinessException(HttpStatus status, ErrMsg msg){
        super(msg.getMessage());
        this.errMsg = msg;
        this.code = status.value();
    }

    public BusinessException(HttpStatus status, String msg){
        super(msg);
        this.errMsg = ErrMsg.build(msg);
        this.code = status.value();
    }

    public BusinessException(ErrMsg msg){
        super(msg.getMessage());
        this.errMsg = msg;
        this.code = 500;
    }

}

package org.lboot.core.auth;

/**
 * 鉴权数据接口
 * @author kindear
 */
public interface AuthDataService {
    /**
     * 获取用户唯一ID
     * @return 用户ID
     */
    String getUid();

    /**
     * 获取会话令牌
     * @return 用户令牌TOKEN
     */
    default String getToken(){
        return "token";
    }
}

package org.lboot.core.auth;

/**
 * @author kindear
 * 风险控制相关接口
 */
public interface RiskAuthService {
    /**
     * 拓展实现禁止操作
     * @param id
     * @param hour 单位是 h 小时
     */
    default void doBan(Object id,Integer hour){

    }

    /**
     * 是否处于禁止状态
     * @param id
     * @return
     */
    default Boolean isBan(Object id){
        return false;
    }
}

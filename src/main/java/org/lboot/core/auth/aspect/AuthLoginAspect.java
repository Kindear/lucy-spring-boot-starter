package org.lboot.core.auth.aspect;


import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.lboot.core.auth.AuthService;
import org.lboot.core.domain.ErrMsg;
import org.lboot.core.exception.BusinessException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

@Slf4j
@Aspect
@Component
public class AuthLoginAspect {

    @Autowired
    AuthService authService;

    /**
     * 以自定义注解作为切入
     */
    @Pointcut("@annotation(org.lboot.core.auth.anno.AuthLogin)")
    public void AuthLogin(){}

    @Before("AuthLogin()")
    public void doBefore(JoinPoint joinPoint) {
        if (!authService.isLogin()){
            throw new BusinessException(HttpStatus.UNAUTHORIZED,ErrMsg.build("未登录"));
        }
    }
}

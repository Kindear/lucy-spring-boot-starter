package org.lboot.core.auth.anno;

import java.lang.annotation.*;

/**
 * @author kindear
 * 权限检查
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD,ElementType.TYPE})
public @interface CheckPerm {
    // 校验权限
    String value() default "";

    // 或拥有某种角色
    String orRole() default "";
}

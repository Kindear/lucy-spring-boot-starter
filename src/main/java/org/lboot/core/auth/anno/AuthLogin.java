package org.lboot.core.auth.anno;

import java.lang.annotation.*;

/**
 * 判断是否处于登录态
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD,ElementType.TYPE})
public @interface AuthLogin {
    String value() default "";

}
